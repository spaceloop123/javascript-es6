const assert = require('assert');
const tasks = require('../src');

describe('strings-tasks', function() {

    it.optional('getStringFromTemplate should create a string from template using given parameters', function() {
        assert.equal('Hello, John Doe!', tasks.getStringFromTemplate('John','Doe'));
        assert.equal('Hello, Chuck Norris!', tasks.getStringFromTemplate('Chuck','Norris'));
    });

    it.optional('repeatString should repeat string specified number of times', function() {
        assert.equal('AAAAA', tasks.repeatString('A', 5));
        assert.equal('catcatcat', tasks.repeatString('cat', 3));
    });

});

describe('03-date-tasks', function() {

    it.optional('isLeapYear should true if specified year is leap', function () {
        [
            new Date(2000,1,1),
            new Date(2012,1,1)
        ].forEach(date => {
            assert(
                tasks.isLeapYear(date) == true,
                `${date} is a leap year`
            );
        });

        [
            new Date(1900,1,1),
            new Date(2001,1,1)
        ].forEach(date => {
            assert(
                tasks.isLeapYear(date) == false,
                `${date} is not a leap year`
            );
        });

    });


    it.optional('timeSpanToString should return the string represation of time span between two dates', function () {
        [
            {
                startDate: new Date(2000,1,1,10,0,0),
                endDate:   new Date(2000,1,1,11,0,0),
                expected:  '01:00:00.000'
            }, {
            startDate: new Date(2000,1,1,10,0,0),
            endDate:   new Date(2000,1,1,10,30,0),
            expected:  '00:30:00.000'
        }, {
            startDate: new Date(2000,1,1,10,0,0),
            endDate:   new Date(2000,1,1,10,0,20),
            expected:  '00:00:20.000'
        }, {
            startDate: new Date(2000,1,1,10,0,0),
            endDate:   new Date(2000,1,1,10,0,0,250),
            expected:  '00:00:00.250'
        }, {
            startDate: new Date(2000,1,1,10,0,0),
            endDate:   new Date(2000,1,1,15,20,10,453),
            expected:  '05:20:10.453'
        }
        ].forEach(data => {
            assert.equal(
                data.expected,
                tasks.timeSpanToString(data.startDate, data.endDate)
            );
        });

    });

});
