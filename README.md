# JavaScript ES6

## Tasks

#### Template string
Returns the result of string template and given parameters firstName and lastName.
For example:
```js
'John','Doe'      => 'Hello, John Doe!'
'Chuck','Norris'  => 'Hello, Chuck Norris!'
```
Write your code in `src/index.js` within an appropriate function `getStringFromTemplate`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### Repeat string
Return a string that repeated the specified number of times.
For example:
```js
'A', 5  => 'AAAAA'
'cat', 3 => 'catcatcat'
```
Write your code in `src/index.js` within an appropriate function `repeatString`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### Leap year
Return true if specified date is leap year and false otherwise.
For example:
```js
Date(1900,1,1)    => false
Date(2000,1,1)    => true
Date(2001,1,1)    => false
Date(2012,1,1)    => true
Date(2015,1,1)    => false
```
Write your code in `src/index.js` within an appropriate function `isLeapYear`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

#### Timespan
Return the string representation of the timespan between two dates.
The format of output string is "HH:mm:ss.sss".
For example:
```js
Date(2000,1,1,10,0,0),  Date(2000,1,1,11,0,0)   => "01:00:00.000"
Date(2000,1,1,10,0,0),  Date(2000,1,1,10,30,0)       => "00:30:00.000"
Date(2000,1,1,10,0,0),  Date(2000,1,1,10,0,20)        => "00:00:20.000"
Date(2000,1,1,10,0,0),  Date(2000,1,1,10,0,0,250)     => "00:00:00.250"
Date(2000,1,1,10,0,0),  Date(2000,1,1,15,20,10,453)   => "05:20:10.453"
```
Write your code in `src/index.js` within an appropriate function `timeSpanToString`
*All test cases are designed as “error-free”, so don't worry about handling any errors.*

## Prepare and test
1. Install [Node.js](https://nodejs.org/en/download/)   
2. Fork this repository: javascript-es6
3. Clone your newly created repo: https://gitlab.com/<%your_gitlab_username%>/javascript-es6/  
4. Go to folder `javascript-es6`  
5. To install all dependencies use [`npm install`](https://docs.npmjs.com/cli/install)  
6. Run `npm test` in the command line  
7. You will see the number of passing and failing tests you 100% of passing tests is equal to 100p in score  

## Submit to [AutoCode](https://frontend-autocode-release-1-5-0-qa.demo.edp-epam.com/)
1. Open [AutoCode](https://frontend-autocode-release-1-5-0-qa.demo.edp-epam.com/) and login
2. Navigate to the JavaScript Mentoring Course page
3. Select your task (javascript-es6)
4. Press the submit button and enjoy, results will be available in few minutes

### Notes
1. We recommend you to use nodejs of version 12 or lower. If you using are any of the features which are not supported by v12, the score won't be submitted.
2. Each of your test case is limited to 30 sec.
